#!/bin/bash

if test "$SECRETS_BUCKET_NAME" -a "$ENVIRONMENT"; then
    # Load the S3 secrets file contents into the environment variables
    eval $(aws s3 cp s3://${SECRETS_BUCKET_NAME}/django_imss-${ENVIRONMENT}.env - | sed 's/^/export /')
fi

eval $@

