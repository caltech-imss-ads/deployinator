RAWVERSION = $(filter-out __version__ = , $(shell grep __version__ deployinator/__init__.py))
VERSION = $(strip $(shell echo $(RAWVERSION)))

PACKAGE = deployinator
DOCKER_REGISTRY = 467892444047.dkr.ecr.us-west-2.amazonaws.com/caltech-imss-ads

clean:
	rm -rf *.tar.gz dist *.egg-info *.rpm
	find . -name "*.pyc" -exec rm '{}' ';'

version:
	@echo $(VERSION)

image_name:
	@echo ${DOCKER_REGISTRY}/${PACKAGE}:${VERSION}

dist: clean
	@python setup.py sdist

build:
	docker build -t ${PACKAGE}:${VERSION} .
	docker tag ${PACKAGE}:${VERSION} ${PACKAGE}:latest

codebuild:
	bin/pre-get-dependencies.sh requirements.Docker.txt && mv requirements.Docker.txt.new requirements.Docker.txt
	docker build -t ${PACKAGE}:${VERSION} .
	docker tag ${PACKAGE}:${VERSION} ${PACKAGE}:latest

tag: build
	docker tag ${PACKAGE}:${VERSION} ${DOCKER_REGISTRY}/${PACKAGE}:${VERSION}
	docker tag ${PACKAGE}:latest ${DOCKER_REGISTRY}/${PACKAGE}:latest

push: tag
	docker push ${DOCKER_REGISTRY}/${PACKAGE}

dev:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml up -d

logall:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml logs

log:
	docker logs imss

docker-clean:
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml stop
	docker-compose -f docker-compose.yml -f docker-compose-dev.yml rm

bootstrap-test:
	bin/deploy.py bootstrap test ${VERSION}

update-test:
	bin/deploy.py update test ${VERSION}

sync-images-to-dev:
	aws s3 sync --acl public-read 's3://caltech-imss-website-v3-storage' 's3://glenn-imss-website-storage.cloud.caltech.edu'

sync-images-to-test:
	aws s3 sync --acl public-read 's3://caltech-imss-website-v3-storage' 's3://django-imss-test'

login:
	aws ecr get-login
