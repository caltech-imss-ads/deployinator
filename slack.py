#!/usr/bin/env python
"""
slack.py: a CodePipeline reporter specific to Caltech ADS workflows

DESIGN GOALS:

    * Report enough information about each buildstep that we don't have
      to look in either Bitbucket or the AWS console for information about
      the deployment process
    * We want to be able to wget this one file within a build step and
      use it without having to install any dependencies.  This saves
      time and complexity during the build.  TRY REALLY HARD NOT TO require
      anything outside python core!!

USAGE IN A BUILDSTEP:

    export SLACK_WEBHOOK=<your slack incoming webhook>
    wget --no-check-certificate https://bitbucket.org/caltech-imss-ads/deployinator/raw/prod/slack.py -O slack.py
    python slack.py <subcommand> <flags>


"""


import argparse
import json
import os
import re
import sys
import time
import subprocess
try:
    # Python 3 specific libraries
    from urllib.parse import urlparse, urlencode
    from http.client import HTTPSConnection
except ImportError:
    # Python 2 specific libraries
    from urlparse import urlparse
    from urllib import urlencode
    from httplib import HTTPSConnection

class Git(object):
    """
    Get a bunch of info about the git repository we're sitting in and save
    the output to a file as JSON.  We save it to a file so that this can be
    bundled up in the zip artifact and passed to later stages of the build.
    """

    INFO_FILE = 'gitinfo.json'

    @staticmethod
    def bitbucket_sha_url(team, repo_name, sha):
        return u"<https://bitbucket.org/{}/{}/commits/{}|{}>".format(
            team,
            repo_name.lower(),
            sha,
            sha
        )

    @staticmethod
    def bitbucket_compare_url(team, repo_name, sha, last_sha):
        return u"<https://bitbucket.org/{}/{}/branches/compare/{}..{}|{}>".format(
            team,
            repo_name.lower(),
            sha,
            last_sha,
            sha
        )

    def __get_long_sha(self):
        return subprocess.check_output(['git', 'rev-parse', 'HEAD']).strip()[1:-1].decode('utf8')

    def __get_short_sha(self):
        return subprocess.check_output(['git', 'log', '--pretty=format:"%h"', '-n', '1']).strip()[1:-1].decode('utf8')

    def __get_committer(self):
        return subprocess.check_output(['git', 'show', '-s', "--format='%aN <%aE>'", 'HEAD']).strip()[1:-1].decode('utf8')

    def __get_branch(self):
        return subprocess.check_output(['git', 'rev-parse', '--abbrev-ref', 'HEAD']).strip().decode('utf8')

    def __get_repo_name(self):
        lines = subprocess.check_output(['git', 'remote', '-v']).split(b'\n')
        for line in lines:
            if b"fetch" in line:
                name = line.split()[1].split(b'/')[-1]
                if name.endswith(b'.git'):
                    name = name[:-4]
                return name.decode('utf8')

    def __get_last_version_sha(self):
        # Get the shas for all tags ordered by reverse date
        tags = subprocess.check_output(['git', 'log', '--tags', '--simplify-by-decoration', '--pretty="%h|%d"', '-n', '10']).split(b'\n')[:-1]
        # filter out merges, which show up in this list
        tags = [t[1:].split(b'|') for t in tags if b"tag:" in t]
        if tags[0][0].decode('utf8') == self.__get_short_sha():
            return tags[1][0].decode('utf8')
        else:
            return tags[0][0].decode('utf8')

    def __get_changelog(self):
        output = subprocess.check_output(['git', 'log', '--pretty=format:"%h||%cr||%s||%an"', '{}...HEAD'.format(self.__get_last_version_sha())]).split(b'\n')
        return [line.decode('utf8')[1:-1] for line in output]

    def save(self):
        info = {
            "long_sha": self.__get_long_sha(),
            "short_sha": self.__get_short_sha(),
            "last_sha": self.__get_last_version_sha(),
            "committer": self.__get_committer(),
            "branch": self.__get_branch(),
            "name": self.__get_repo_name(),
            "changelog": self.__get_changelog()
        }
        self.__get_changelog()
        with open(self.INFO_FILE, "w+") as f:
            f.write(json.dumps(info))

    def load(self):
        with open(self.INFO_FILE) as f:
            info = json.loads(f.read())
        return info


class DockerImage(object):
    """
    Look for the docker image we just built and return some
    info about it
    """

    def __get_image_id(self, image):
        line = str(subprocess.check_output(['docker', 'images', image]).split(b'\n')[1])
        return re.split(r'\s{2,}', line)[2]

    def __get_image_size(self, image):
        line = str(subprocess.check_output(['docker', 'images', image]).split(b'\n')[1])
        return re.split(r'\s{2,}', line)[4]

    def __get_image_region(self, image):
        line = str(subprocess.check_output(['docker', 'images', image]).split(b'\n')[1])
        return re.split(r'\s{2,}', line)[4]

    def load(self, image):
        return {
            'image': image,
            'size': self.__get_image_size(image),
            'id': self.__get_image_id(image),
        }


class CodeBuildEnvironment(object):
    """
    When running in CodeBuild, return some info about
    the environment.
    """

    def build_url(self):
        info = self.load()
        return "<https://{}.console.aws.amazon.com/codebuild/home?region={}#/builds/{}/view/new|Click here>".format(
            info['region'],
            info['region'],
            info['build_id'],
        )

    def load(self):
        return {
            'region': os.environ['AWS_REGION'],
            'build_id': os.environ['CODEBUILD_BUILD_ID'],
        }


class Slack(object):

    def __init__(self):
        url = os.environ['SLACK_WEBHOOK']
        parts = urlparse(url)
        self.server = parts.netloc
        self.path = parts.path

    def send(self, payload):
        """
        Send payload to slack API
        """
        h = HTTPSConnection(self.server)
        headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
        data = urlencode({'payload': json.dumps(payload)})
        h.request('POST', self.path, data, headers)
        r = h.getresponse()
        response = r.read()
        return response.decode('utf-8')


class SimpleFormatter(object):
    """
    Send a simple Slack message with no attachments.
    """

    def format(self, args):
        payload = {
            'text': args.message,
            'mrkdown': "true",
            'username': args.username,
            'channel': args.channel,
            'icon_emoji': args.icon,
        }
        if args.pretext:
            payload['pretext'] = args.pretext
        return payload


class PrettyFormatter(object):
    """
    Send a more complicated Slack message with an attachment.
    """

    def format(self, args):
        attachment = {
            'fallback': 'Content attached',
            'mrkdown': "true",
            'text': args.message,
            'color': args.color,
            'author_icon': args.icon
        }
        payload = {
            'mrkdown': "true",
            'username': args.username,
            'channel': args.channel,
            'attachments': [attachment],
            'text': args.pretext,
        }
        return payload


class BitbucketPipelinesFormatter(object):
    """
    Send a detailed message about the bitbucket step.
    """

    def prep(self):
        Git().save()

    def format(self, args):
        info = Git().load()

        title = " *{}-{}*: *bitbucket-pipeline*: zip files pushed to S3".format(info['name'], args.version)

        attachments = []
        attachment = {
            'fallback': 'Content attached',
            'mrkdown': "true",
            'color': 'good',
            'fields': [
                {
                    'title': 'Branch',
                    'value': info['branch'],
                    'short': True,
                },
                {
                    'title': 'Version',
                    'value': args.version,
                    'short': True,
                },
                {
                    'title': 'Committer',
                    'value': info['committer'],
                    'short': False,
                }

            ],
            'ts': int(time.time())

        }
        attachments.append(attachment)
        changelog = []
        for line in info['changelog']:
            try:
                sha, date, msg, committer = line.split('||')
                line = u"{} [{}] {} - {}".format(
                    Git.bitbucket_sha_url(args.team, info['name'], sha),
                    date,
                    msg,
                    committer
                )
                changelog.append(line)
            except ValueError:
                pass
        attachment = {
            'fallback': 'Content attached',
            'mrkdown': "true",
            'color': '#aaaaaa',
            'title': 'Changes in version {}'.format(args.version),
            'text': "\n".join(changelog)
        }
        attachments.append(attachment)
        payload = {
            'mrkdown': "true",
            'username': args.username,
            'channel': args.channel,
            'attachments': attachments,
            'text': title,
        }
        return payload

class DeployFormatter(object):

    def fail(self, args):
        git_info = Git().load()
        title = " *{}-{}*: *Deploy*: ".format(git_info['name'], args.version)

        attachments = []
        attachment = {
            'fallback': 'Content attached',
            'mrkdown': "true",
            'color': 'danger',
            'fields': [
                {
                    'title': 'Status',
                    'value': "FAIL",
                    'short': True,
                },
                {
                    'title': 'Build info',
                    'value': CodeBuildEnvironment().build_url(),
                    'short': True,
                },
                {
                    'title': 'Git info',
                    'value': "{} ({}) {}".format(
                        git_info['branch'],
                        Git.bitbucket_compare_url(args.team, git_info['name'], git_info['short_sha'], git_info['last_sha']),
                        git_info['committer']
                    ),
                    'short': False,
                }
            ],
            'ts': int(time.time())

        }
        attachments.append(attachment)
        payload = {
            'mrkdown': "true",
            'username': args.username,
            'channel': args.channel,
            'attachments': attachments,
            'text': title,
        }

    def success(self, args):
        git_info = Git().load()
        docker_info = DockerImage().load(args.image)
        title = " *{}-{}*: *Build*: build and push docker image".format(git_info['name'], args.version)

        attachments = []
        attachment = {
            'fallback': 'Content attached',
            'mrkdown': "true",
            'color': 'good',
            'fields': [
                {
                    'title': 'Status',
                    'value': "Success",
                    'short': True,
                },
                {
                    'title': 'Build info',
                    'value': CodeBuildEnvironment().build_url(),
                    'short': True,
                },
                {
                    'title': 'Git info',
                    'value': "{} ({}) {}".format(
                        git_info['branch'],
                        Git.bitbucket_compare_url(args.team, git_info['name'], git_info['short_sha'], git_info['last_sha']),
                        git_info['committer']
                    ),
                    'short': False,
                },
                {
                    'title': 'Image size',
                    'value': docker_info['size'],
                    'short': True,
                },
                {
                    'title': 'Image id',
                    'value': docker_info['id'],
                    'short': False,
                }
            ],
            'ts': int(time.time())

        }
        attachments.append(attachment)
        payload = {
            'mrkdown': "true",
            'username': args.username,
            'channel': args.channel,
            'attachments': attachments,
            'text': title,
        }
        return payload



class DockerBuildFormatter(object):
    """
    Send a detailed message about the results of a Docker build.
    """

    def fail(self, args):
        git_info = Git().load()
        title = " *{}-{}*: *Build*: build and push docker image".format(git_info['name'], args.version)

        attachments = []
        attachment = {
            'fallback': 'Content attached',
            'mrkdown': "true",
            'color': 'danger',
            'fields': [
                {
                    'title': 'Status',
                    'value': "FAIL",
                    'short': True,
                },
                {
                    'title': 'Build info',
                    'value': CodeBuildEnvironment().build_url(),
                    'short': True,
                },
                {
                    'title': 'Git info',
                    'value': "{} ({}) {}".format(
                        git_info['branch'],
                        Git.bitbucket_compare_url(args.team, git_info['name'], git_info['short_sha'], git_info['last_sha']),
                        git_info['committer']
                    ),
                    'short': False,
                }
            ],
            'ts': int(time.time())

        }
        attachments.append(attachment)
        payload = {
            'mrkdown': "true",
            'username': args.username,
            'channel': args.channel,
            'attachments': attachments,
            'text': title,
        }
        return payload

    def success(self, args):
        git_info = Git().load()
        docker_info = DockerImage().load(args.image)
        title = " *{}-{}*: *Build*: build and push docker image".format(git_info['name'], args.version)

        attachments = []
        attachment = {
            'fallback': 'Content attached',
            'mrkdown': "true",
            'color': 'good',
            'fields': [
                {
                    'title': 'Status',
                    'value': "Success",
                    'short': True,
                },
                {
                    'title': 'Build info',
                    'value': CodeBuildEnvironment().build_url(),
                    'short': True,
                },
                {
                    'title': 'Git info',
                    'value': "{} ({}) {}".format(
                        git_info['branch'],
                        Git.bitbucket_compare_url(args.team, git_info['name'], git_info['short_sha'], git_info['last_sha']),
                        git_info['committer']
                    ),
                    'short': False,
                },
                {
                    'title': 'Image size',
                    'value': docker_info['size'],
                    'short': True,
                },
                {
                    'title': 'Image id',
                    'value': docker_info['id'],
                    'short': False,
                }
            ],
            'ts': int(time.time())

        }
        attachments.append(attachment)
        payload = {
            'mrkdown': "true",
            'username': args.username,
            'channel': args.channel,
            'attachments': attachments,
            'text': title,
        }
        return payload

    def prep(self):
        pass

    def format(self, args):
        if args.status == "success":
            return self.success(args)
        return self.fail(args)


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--user', dest="username", default="deployinator")
    parser.add_argument('--channel', dest="channel", default="#ads_deploys")
    subparsers = parser.add_subparsers(dest="subparser_name")

    simple_parser = subparsers.add_parser('simple')
    simple_parser.add_argument('--title', dest="pretext", default=None, metavar='TITLE')
    simple_parser.add_argument('--icon', help='Icon to use for the message. See http://www.webpagefx.com/tools/emoji-cheat-sheet/ for your choices', dest="icon", default=':octopus:')
    simple_parser.add_argument('message')

    pretty_parser = subparsers.add_parser('pretty')
    pretty_parser.add_argument('--title', dest="pretext", default=None, metavar='TITLE')
    pretty_parser.add_argument('--color', help='Color for the sidebar of the attachment. Specify hex or one of "good", "warning", "danger"', dest="color", default='good')
    pretty_parser.add_argument('--icon', help='Icon to use for the message. See http://www.webpagefx.com/tools/emoji-cheat-sheet/ for your choices', dest="icon", default=':octopus:')
    pretty_parser.add_argument('--field', help='Add a field to the attachment', nargs='*', dest='fields', metavar='TITLE:VALUE')
    pretty_parser.add_argument('message')

    bitbucket_parser = subparsers.add_parser('bitbucket')
    bitbucket_parser.add_argument('--prep', help='prepare for sending the bitbucket message', dest='prep', action="store_true", default=False)
    bitbucket_parser.add_argument('--version', help='what version of our software are we releasing?', dest='version', metavar='VERSION')
    bitbucket_parser.add_argument('--team', help='What bitbucket team owns our repository?', default='caltech-imss-ads', dest='team', metavar='TEAM')

    docker_build_parser = subparsers.add_parser('docker')
    docker_build_parser.add_argument('--prep', help='prepare for sending the docker build message', dest='prep', action="store_true", default=False)
    docker_build_parser.add_argument('--version', help='what version of our software are we releasing?', dest='version', metavar='VERSION')
    docker_build_parser.add_argument('--image', help='what docker image?', dest='image', metavar='IMAGE_TAG')
    docker_build_parser.add_argument('--team', help='What bitbucket team owns our repository?', default='caltech-imss-ads', dest='team', metavar='TEAM')
    docker_build_parser.add_argument('--status', help='Did this build succeed or fail?', default='success', dest='status', metavar='STATUS')

    return parser.parse_args()


def main():

    args = parse_arguments()

    if args.subparser_name == "simple":
        formatter = SimpleFormatter()
        response = Slack().send(SimpleFormatter().format(args))
    elif args.subparser_name == 'pretty':
        formatter = PrettyFormatter()
    elif args.subparser_name == 'bitbucket':
        if args.prep:
            BitbucketPipelinesFormatter().prep()
            return 0
        formatter = BitbucketPipelinesFormatter()
    elif args.subparser_name == 'docker':
        if args.prep:
            DockerBuildFormatter().prep()
            return 0
        formatter = DockerBuildFormatter()
    elif args.subparser_name == 'deploy':
        if args.prep:
            DeployFormatter().prep()
            return 0
        formatter = DeployFormatter()
    response = Slack().send(formatter.format(args))
    if response != 'ok':
        return 1
    return 0

if __name__ == '__main__':
    sys.exit(main())
