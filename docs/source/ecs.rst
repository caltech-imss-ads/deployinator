*************************
Elastic Container Service
*************************

Service
=======

.. autoclass:: deployinator.aws.ecs.Service
   :members:

Task
====

.. autoclass:: deployinator.aws.ecs.Task
   :members:

TaskDefinition
==============

.. autoclass:: deployinator.aws.ecs.TaskDefinition
   :members:

ContainerDefinition
===================

.. autoclass:: deployinator.aws.ecs.ContainerDefinition
   :members:

VolumeMixin
===========

.. autoclass:: deployinator.aws.ecs.VolumeMixin
   :members:

