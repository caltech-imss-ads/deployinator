# ========================================================================================
#
# module: site
#
# Set up most of the infrastructure for a generic webserver site
#
#  * ELB, listening on 80 and 443
#  * Autoscaling group, with CPU based scale up/scale down triggers
#  * IAM task role for the ECS service
#
# ========================================================================================


variable "site" {
  default = ""
  description = "Name of the site, e.g. hss, bbe, cce, etc."
}

variable "environment" {
  default = ""
  description = "'prod', 'test'"
}

variable "config_store_ro_policy_arn" {
  default = ""
  description = "The ARN of the IAM policy for readonly access to the config store for the environment."
}

variable "logging_bucket_name" {
  default = ""
  description = "The name of the S3 bucket to which the ELB and the site resource bucket will write their logs"
}

variable "http_port" {
  default = ""
  description = "The HTTP port on the container; this will be different for each site.  We will map port 80 on the ELB to this port."
}

variable "https_port" {
  default = ""
  description = "The HTTPS port on the container; this will be different for each site.  We will map port 443 on the ELB to this port."
}

variable "elb_subnet_ids" {
  type = "list"
  default = []
  description = "This is a list of IDs of subnets into which to launch the ELB instances themselves."
}

variable "elb_security_group_ids" {
  type = "list"
  default = []
  description = "This is a list of security_group_ids to apply to the ELB."
}

variable "certificate_arn" {
  default = "arn:aws:iam::467892444047:server-certificate/self-signed"
  description = "The ARN of the SSL certificate to use for the HTTPS portion of the ELB.  You can get this by doing 'aws iam list-server-certificates'."
}

variable "ecs_cluster_name" {
  default = ""
  description = "Name of the preexisting ECS cluster to which to add our autoscaling group instances."
}

variable "ecs_aws_key_name" {
  default = ""
  description = "The AWS id of the ssh key pair to use for creating the ECS cluster instances." 
}

variable "ecs_ami_id" {
  default = ""
  description = "The AMI id to use for the ECS cluster instances." 
}

variable "ecs_instance_profile" {
  default = ""
  description = "The instance profile id to use for the ECS cluster instances." 
}

variable "ecs_instance_type" {
  default = ""
  description = "The instance type to use for the ECS cluster instances." 
}

variable "ecs_subnet_ids" {
  type = "list"
  default = []
  description = "A list of IDs of subnets into which to launch the ECS cluster instances." 
}

variable "ecs_security_group_ids" {
  type = "list"
  default = []
  description = "A list of IDs of security groups to apply to the ECS cluster instances." 
}

variable "ecs_autoscaling_min_size" {
  default = ""
  description = "The min size for our ECS cluster autoscaling group." 
}

variable "ecs_autoscaling_max_size" {
  default = ""
  description = "The max size for our ECS cluster autoscaling group." 
}

variable "group" {
  default = "ADS"
  description = "Which IMSS group owns the resources in this site: ACS or ADS." 
}

# =============================
# ELB
# =============================

# Here we configure the ELB for our site.
#
# For shibd to work, we need to ensure that someone who logs in over HTTPS
# always sticks to the same ELB host, else either the IdP or shibd freaks out
# and thinks that the person's shib session is invalid.  Recall that ELBs have
# multiple front end hosts, typically one per AZ for small sites.
#
#  * We map the site specific HTTP port to port 80 on the ELB
#  * We map the site specific HTTPS port to port 443 on the ELB
#  * We configure the health check to check HTTP:${container_http_port}/
#  * We configure the ELB logs to go to our logging bucket

resource "aws_elb" "deployinator" {
  name            = "${var.site}-${var.environment}"
  subnets         = ["${var.elb_subnet_ids}"]
  security_groups = ["${var.elb_security_group_ids}"]

  listener {
    instance_port     = "${var.http_port}"
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port     = "${var.https_port}"
    instance_protocol = "https"
    lb_port           = 443
    lb_protocol       = "https"
    ssl_certificate_id = "${var.certificate_arn}"
  }


  access_logs {
    bucket        = "${var.logging_bucket_name}"
    bucket_prefix = "${var.site}-${var.environment}-elb"
    interval      = 5
  }

  health_check {
    healthy_threshold   = 3
    unhealthy_threshold = 8
    timeout             = 60
    target              = "HTTP:${var.http_port}/"
    interval            = 72
  }

  cross_zone_load_balancing   = true
  idle_timeout                = 60
  connection_draining         = true
  connection_draining_timeout = 300

  tags {
    Name = "${var.site}-${var.environment}"
    project = "infrastructure"
    Environment = "${var.environment}"
    Group = "${var.group}"
  }
}

# =================
# Autoscaling Group
# =================

data "template_file" "userdata" {
  template = "${file("${path.module}/userdata.sh")}"

  vars {
    cluster_name    = "${var.ecs_cluster_name}"
    environment     = "${var.environment}"
  }
}

resource "aws_launch_configuration" "deployinator" {
  lifecycle {
    create_before_destroy = true
  }

  name_prefix          = "${var.site}-${var.environment}"
  image_id             = "${var.ecs_ami_id}"
  instance_type        = "${var.ecs_instance_type}"
  user_data            = "${data.template_file.userdata.rendered}"
  security_groups      = ["${var.ecs_security_group_ids}"]
  iam_instance_profile = "${var.ecs_instance_profile}"
  key_name             = "${var.ecs_aws_key_name}"
}

resource "aws_autoscaling_group" "deployinator" {
  lifecycle {
    create_before_destroy = true
  }

  name                      = "${var.site}-${var.environment}"
  vpc_zone_identifier       = ["${var.ecs_subnet_ids}"]
  max_size                  = "${var.ecs_autoscaling_max_size}"
  min_size                  = "${var.ecs_autoscaling_min_size}"
  health_check_grace_period = 300
  force_delete              = true
  launch_configuration      = "${aws_launch_configuration.deployinator.name}"

  tag {
    key                 = "Type"
    value               = "${var.site}-${var.environment}"
    propagate_at_launch = false
  }

  tag {
    key                 = "project"
    value               = "${var.site}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Environment"
    value               = "${var.environment}"
    propagate_at_launch = true
  }

  tag {
    key                 = "Group"
    value               = "${var.group}"
    propagate_at_launch = true
  }
}

# ====================
# ASG Scaling Policies
# ====================

# Add one instance to the autoscaling group; do it at most
# once per minute.

resource "aws_autoscaling_policy" "scale-up" {
  name                    = "${var.site}-${var.environment}-scale-up"
  scaling_adjustment      = 1
  adjustment_type         = "ChangeInCapacity"
  cooldown                = 60
  autoscaling_group_name  = "${aws_autoscaling_group.deployinator.name}"
}

# Remove one instance from the autoscaling group; do it at most
# once per minute.

resource "aws_autoscaling_policy" "scale-down" {
  name                    = "${var.site}-${var.environment}-scale-down"
  scaling_adjustment      = -1
  adjustment_type         = "ChangeInCapacity"
  cooldown                = 60
  autoscaling_group_name  = "${aws_autoscaling_group.deployinator.name}"
}

# ==========================================
# Cloudwatch Alarms for the scaling policies
# ==========================================

# Check the average CPU for the autoscaling group every minute; if 
# average cpu is > 60 for 5 minutes, enter alarm state and execute
# the aws_autoscaling_policy.scale-up action

resource "aws_cloudwatch_metric_alarm" "cpu-high" {
  alarm_name            = "${var.site}-${var.environment}-cpu-high"
  comparison_operator   = "GreaterThanOrEqualToThreshold"
  evaluation_periods    = "5"
  metric_name           = "CPUUtilization"
  namespace             = "AWS/EC2"
  period                = "60"
  statistic             = "Average"
  threshold             = "60"
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.deployinator.name}"
  }
  alarm_description     = "Alarm for ${var.site}-${var.environment} ASG average CPU > 60% for 5 mins"
  alarm_actions         = ["${aws_autoscaling_policy.scale-up.arn}"]
}

# Check the average CPU for the autoscaling group every minute; if 
# average cpu is < 60 for 20 minutes, enter alarm state and execute
# the aws_autoscaling_policy.scale-down action

resource "aws_cloudwatch_metric_alarm" "cpu-low" {
  alarm_name            = "${var.site}-${var.environment}-cpu-low"
  comparison_operator   = "LessThanOrEqualToThreshold"
  evaluation_periods    = "20"
  metric_name           = "CPUUtilization"
  namespace             = "AWS/EC2"
  period                = "60"
  statistic             = "Average"
  threshold             = "30"
  dimensions {
    AutoScalingGroupName = "${aws_autoscaling_group.deployinator.name}"
  }
  alarm_description     = "Alarm for ${var.site}-${var.environment} ASG average CPU < 30% for 20 mins"
  alarm_actions         = ["${aws_autoscaling_policy.scale-down.arn}"]
}

# ==============
# IAM Task Role
# ==============

# Here we configure the IAM role for our ECS tasks.  The role
# is what allows the task to use temporary AWS credentials to
# be able to use the IAM policies attached to the role

resource "aws_iam_role" "deployinator-task-role" {
  name = "${var.site}-${var.environment}-task"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

# This attachment allows the role readonly access to the config store S3 bucket
resource "aws_iam_role_policy_attachment" "deployinator-config-store-ro" {
  role       = "${aws_iam_role.deployinator-task-role.id}"
  policy_arn = "${var.config_store_ro_policy_arn}"
}

# =================
# KMS
# =================

resource "aws_kms_key" "deployinator" {
    description = "${var.site}-${var.environment} KMS key"
    tags {
        project = "infrastructure"
        Environment = "${var.environment}"
        Group = "${var.group}"
    }
}

resource "aws_kms_alias" "deployinator" {
    name = "alias/${var.site}-${var.environment}"
    target_key_id = "${aws_kms_key.deployinator.id}"
}

# =================
# Outputs
# =================

# These outputs are what allow terraform code outside this module
# to get to values of things inside this module

output "elb-id" {
	value = "${aws_elb.deployinator.id}"
}

output "iam-task-role-arn" {
	value = "${aws_iam_role.deployinator-task-role.arn}"
}

output "http-port" {
    value = "${var.http_port}"
}

output "https-port" {
    value = "${var.https_port}"
}

output "ecs-cluster-name" {
    value = "${var.ecs_cluster_name}"
}

output "ecs-autoscalinggroup-name" {
    value = "${aws_autoscaling_group.deployinator.id}"
}

output "kms-key-arn" {
    value = "${aws_kms_key.deployinator.arn}"
}

output "kms-key-alias" {
    value = "${aws_kms_alias.deployinator.name}"
}
