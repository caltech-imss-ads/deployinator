#!/bin/bash
/usr/bin/name_myself.py ecs
/usr/bin/aws-whoami

echo ECS_CLUSTER=${cluster_name} >> /etc/ecs/ecs.config
rm -f /var/lib/ecs/data/ecs_agent_data.json
sed -i -e "s/environment test/environment ${environment}/g" /etc/td-agent/td-agent.conf
service td-agent restart
