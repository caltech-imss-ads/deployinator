variable "region" {
  default = "us-west-2"
}

variable "aws_key_name" {
  default = "adskey1"
}

provider "aws" {
  region = "${var.region}"
}

# =====================
# VPC Remote State File
# =====================
data "terraform_remote_state" "vpc" {
  backend = "s3"

  config {
    bucket = "caltech-terraform-remotestate-file"
    key    = "ads-web-terraform-state"
    region = "us-west-2"
  }
}

# =======================
# ECR Repository
# =======================

resource "aws_ecr_repository" "apache-hello" {
  name = "caltech-imss-ads/apache-hello"
}

# =======================
# ECS Clusters
# =======================

resource "aws_ecs_cluster" "deployinator" {
  name = "deployinator"
}

# ==================
# Sites
# ==================

# See https://www.evernote.com/shard/s348/nl/784611111/f646971d-c832-4915-a2b6-10103ab3a261/
# ("Access.caltech application (and off-site gunicorn) server ports" page in Evernote)
# for the site to port mappings for http_port and https_port.

# When you add a new site, add new outputs (see "Outputs", below) for the
# elb-id, task-role-arn, resource-storage-bucket, ecs-cluster-name and
# ecs-autoscalinggroup-name

module "deployinator-prod" {
  source                     = "./site"
  site                       = "deployinator"
  environment                = "prod"
  config_store_ro_policy_arn = "${data.terraform_remote_state.vpc.s3-test-config-store-iam-ro-arn}"
  logging_bucket_name        = "${data.terraform_remote_state.vpc.s3-logging-bucket}"
  http_port                  = 8204
  https_port                 = 8205
  elb_subnet_ids             = ["${data.terraform_remote_state.vpc.subnet-elb-public-b}", "${data.terraform_remote_state.vpc.subnet-elb-public-c}"]
  elb_security_group_ids     = ["${data.terraform_remote_state.vpc.sg-http-https-from-campus}"]
  ecs_cluster_name           = "${aws_ecs_cluster.deployinator.name}"
  ecs_aws_key_name           = "${var.aws_key_name}"
  ecs_ami_id                 = "${data.terraform_remote_state.vpc.ecs-cloudinit-goldenhost-ami-id}"
  ecs_instance_profile       = "${data.terraform_remote_state.vpc.ecs_ecr_profile}"
  ecs_instance_type          = "t2.small"
  ecs_subnet_ids             = ["${data.terraform_remote_state.vpc.subnet-test-web-servers-private-b}", "${data.terraform_remote_state.vpc.subnet-test-web-servers-private-c}"]
  ecs_security_group_ids     = ["${data.terraform_remote_state.vpc.sg-all-internal-vpc}", "${data.terraform_remote_state.vpc.sg-all-from-autoproxy}"]
  ecs_autoscaling_min_size   = 2
  ecs_autoscaling_max_size   = 4
}


# ====================
# Outputs
# ====================

output "vpc_id" {
    value = "${data.terraform_remote_state.vpc.vpc_id}"
}

output "apache-hello-ecr-repository-url" {
    value = "${aws_ecr_repository.apache-hello.repository_url}"
}

output "ecs-autoscaling-role-arn" {
    value = "${data.terraform_remote_state.vpc.ecs_autoscaling_role_arn}"
}

# deployinator-prod specific output
# ------------------------------

output "s3-deployinator-prod-config-store-bucket" {
    value = "${data.terraform_remote_state.vpc.s3-test-config-store-bucket}"
}

output "deployinator-prod-elb-id" {
	value = "${module.deployinator-prod.elb-id}"
}

output "deployinator-prod-http-port" {
	value = "${module.deployinator-prod.http-port}"
}

output "deployinator-prod-https-port" {
	value = "${module.deployinator-prod.https-port}"
}

output "deployinator-prod-task-role-arn" {
	value = "${module.deployinator-prod.iam-task-role-arn}"
}

output "deployinator-prod-ecs-cluster-name" {
	value = "${module.deployinator-prod.ecs-cluster-name}"
}

output "deployinator-prod-ecs-autoscalinggroup-name" {
	value = "${module.deployinator-prod.ecs-autoscalinggroup-name}"
}

output "deployinator-prod-kms-key-arn" {
	value = "${module.deployinator-prod.kms-key-arn}"
}

output "deployinator-prod-kms-key-alias" {
	value = "${module.deployinator-prod.kms-key-alias}"
}

output "deployinator-prod-rds-address" {
    value = "${data.terraform_remote_state.vpc.rds-prod-address}"
}
